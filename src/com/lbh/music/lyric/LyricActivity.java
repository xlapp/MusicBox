package com.lbh.music.lyric;

import java.util.List;
import java.util.Map;

import com.lbh.music.activity.Music;
import com.lbh.music.activity.MusicList;
import com.lbh.music.activity.PreferenceService;
import com.lbh.music.service.MusicService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.lbh.music.R;

/**
 * 歌词
 */
@SuppressLint("HandlerLeak")
public class LyricActivity extends Activity {
	public LrcView lrc_view;
	public LrcProcess mLrcProcess;
	public LrcView mLrcView;
	public LinearLayout nolrc;
	ListView erllist;
	TextView shoudonglrc;
	private MusicPlay6er receiver9;
	private LrcDownFinish lrcReceive;
	private MyProgressBroadCa receiver;
	PreferenceService service;
	private TextView tvword;
	String word = "";
	private String Strsinger;
	private String Strmusic;
	// 创建对象
	// 初始化歌词检索值
	private List<Music> lists;
	String[] lrc = new String[200];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyricview);
		lrc_view = (LrcView) findViewById(R.id.LyricShow);
		// erllist = (ListView) findViewById(R.id.erllist);
		shoudonglrc = (TextView) findViewById(R.id.shoudonglrc);
		nolrc = (LinearLayout) findViewById(R.id.nolrc);
		tvword = (TextView) findViewById(R.id.word);
		lists = MusicList.getMusicData(getApplicationContext());
		service = new PreferenceService(this);

		receiver9 = new MusicPlay6er();
		IntentFilter filter9 = new IntentFilter("com.cn.musicserviceplayer");
		this.registerReceiver(receiver9, filter9);
		lrcReceive = new LrcDownFinish();
		receiver = new MyProgressBroadCa();
		IntentFilter filter = new IntentFilter("cn.com.karl.progress");
		this.registerReceiver(receiver, filter);

		IntentFilter lrcFilter = new IntentFilter("com.cn.lrcDownFinish");
		this.registerReceiver(lrcReceive, lrcFilter); // 注册歌词下载广播
		shoudonglrc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(LyricActivity.this, LrcSearch.class);
				startActivity(intent);
			}
		});
	}

	/**
	 * 歌词进度广播
	 */
	public class MyProgressBroadCa extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			lrc_view.SetIndex(LrcIndex.LrcIndex());
			lrc_view.invalidate();
		}
	}

	/**
	 * 歌词下载完毕，更新歌词控件显示
	 */
	public class LrcDownFinish extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// 重新初始化歌词控件
			if (SearchLRC.DOWN_FINISH) {

				Log.e("LyricActivity", "LyricActivity: 歌词下载完毕！");
				Toast.makeText(getApplicationContext(), "歌词下载完毕...",
						Toast.LENGTH_SHORT).show();
				refreshLrc();
			} else {
				Toast.makeText(getApplicationContext(), "=.=！很抱歉，歌词下失败！",
						Toast.LENGTH_SHORT).show();
			}
			SearchLRC.DOWN_FINISH = false;  //让标志位 初始化
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Intent intent = new Intent(LyricActivity.this, MusicService.class);
		// startService(intent);
		refreshLrc();
	}

	/**
	 * 刷新歌词显示
	 */
	public void refreshLrc() {
		Map<String, String> paramslrcurl = service.getPreferenceslrcurl();
		mLrcProcess = new LrcProcess();
		if (lists.size() > MusicService._id) {

			if (!paramslrcurl.get("lrcurledit").equals("")) {
				mLrcProcess = new LrcProcess();
				mLrcProcess.readLRC(paramslrcurl.get("lrcurledit")
						+ lists.get(MusicService._id).getName());
			} else {
				mLrcProcess = new LrcProcess();
				mLrcProcess.readLRC(lists.get(MusicService._id).getUrl());
			}
		}
		LrcIndex.lrcList = mLrcProcess.getLrcContent();
		if (LrcIndex.lrcList.size() <= 0) {
			nolrc.setVisibility(View.VISIBLE);

		} else {
			nolrc.setVisibility(View.GONE);
			lrc_view.setSentenceEntities(LrcIndex.lrcList);
		}
	}

	// prepare完成后
	public class MusicPlay6er extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			new Thread(new Runnable() {
				@Override
				public void run() {
					Looper.prepare();
					// getWords();
					Looper.loop();
				}
			}).start();

			Map<String, String> paramslrcurl = service.getPreferenceslrcurl();
			mLrcProcess = new LrcProcess();

			if (!paramslrcurl.get("lrcurledit").equals("")) {
				mLrcProcess.readLRC(paramslrcurl.get("lrcurledit")
						+ lists.get(MusicService._id).getName());
				Log.i("getUrl",
						paramslrcurl.get("lrcurledit")
								+ lists.get(MusicService._id).getName());
			} else {
				mLrcProcess.readLRC(lists.get(MusicService._id).getUrl());
			}
			LrcIndex.lrcList = mLrcProcess.getLrcContent();
			if (LrcIndex.lrcList.size() <= 0) {
				nolrc.setVisibility(View.VISIBLE);
				lrc_view.setSentenceEntities(LrcIndex.lrcList);
			} else {
				try {
					nolrc.setVisibility(View.GONE);
					lrc_view.setSentenceEntities(LrcIndex.lrcList);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	@Override
	protected void onDestroy() {
		mLrcProcess = null;
		mLrcView = null;
		this.unregisterReceiver(receiver);
		this.unregisterReceiver(lrcReceive);
		this.unregisterReceiver(receiver9);
		super.onDestroy();
	}
	/*
	 * private List<String> getData(){ List<String> data = new
	 * ArrayList<String>(); data.add(""); return data; }
	 */
	/*
	 * @SuppressLint("ShowToast") private void getWords() {
	 * Map<String,String>paramslrcurl=service.getPreferenceslrcurl(); Music
	 * m=lists.get(MusicService._id); Strsinger =m.getSinger(); Strmusic =
	 * m.getTitle(); if(Strsinger == "" || Strmusic == ""){
	 * Toast.makeText(getApplicationContext(), "空", 1).show(); }else{ SearchLRC
	 * search = new SearchLRC(Strmusic, Strsinger);
	 * 
	 * @SuppressWarnings("rawtypes") ArrayList result = search.fetchLyric();
	 * if(result == null){ Log.i("ads", "null"); }else{ word = ""; if
	 * (result.size() > 0) { for (int i = 0; i < result.size()-2; i++) { word +=
	 * result.get(i); word += "\n"; }
	 * DebugMessage.put(paramslrcurl.get("lrcurledit"), word,
	 * m.getName().substring(m.getName().length()-4,
	 * m.getName().length()-3).equals(".") ? m.getName().substring(0,
	 * m.getName().length()-4) : m.getName()); } Message msg =
	 * mHandler.obtainMessage(0); mHandler.sendMessage(msg); } } }
	 */
	/*
	 * private Handler mHandler = new Handler() { public void
	 * handleMessage(Message msg) { switch (msg.what) { case 0: refresh();
	 * break; } } };
	 */
	/*
	 * private void refresh() { tvword.setText(""); tvword.setText(word);
	 * mLrcProcess = new LrcProcess();
	 * Map<String,String>paramslrcurl=service.getPreferenceslrcurl();
	 * if(!paramslrcurl.get("lrcurledit").equals("")){
	 * mLrcProcess.readLRC(paramslrcurl
	 * .get("lrcurledit")+lists.get(MusicService._id).getName()); }else{
	 * mLrcProcess.readLRC(lists.get(MusicService._id).getUrl()); }
	 * LrcIndex.lrcList = mLrcProcess.getLrcContent();
	 * if(LrcIndex.lrcList.size()<=0){ nolrc.setVisibility(View.VISIBLE);
	 * lrc_view.setSentenceEntities(LrcIndex.lrcList); }else{ try{
	 * nolrc.setVisibility(View.GONE);
	 * lrc_view.setSentenceEntities(LrcIndex.lrcList); }catch(Exception e){
	 * e.printStackTrace(); } } }
	 */

}
