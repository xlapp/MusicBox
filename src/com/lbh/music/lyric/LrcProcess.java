package com.lbh.music.lyric;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

/**
 * 处理歌词文件的类
 */
public class LrcProcess {
	private List<LrcContent> LrcList;
	private LrcContent mLrcContent;

	public LrcProcess() {
		mLrcContent = new LrcContent();
		LrcList = new ArrayList<LrcContent>();
	}

	/**
	 * 读取歌词文件的内容
	 */
	public String readLRC(String song_path) {

		StringBuilder stringBuilder = new StringBuilder();
		String name = song_path.replace(".mp3", ".lrc");
		File f = new File(name);
		if (f.exists()) {
			try {
				FileInputStream fis = new FileInputStream(f);
				InputStreamReader isr = new InputStreamReader(fis, "GBK"); // 编码格式
				BufferedReader br = new BufferedReader(isr);
				String s = "";
				while ((s = br.readLine()) != null) {
					s = s.replace("[", "").trim();
					s = s.replace("]", "@").trim();
					// 分离"@"字符
					String splitLrc_data[] = s.split("@");
					if (splitLrc_data.length > 1) {
						mLrcContent.setLrc(splitLrc_data[1]);
						// 处理歌词取得歌曲时间
						int LrcTime = TimeStr(splitLrc_data[0]);
						mLrcContent.setLrc_time(LrcTime);
						// 添加进列表数组
						LrcList.add(mLrcContent);
						// 创建对象
						mLrcContent = new LrcContent();
					}
				}
				br.close();
				isr.close();
				fis.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				stringBuilder.append("");
			} catch (IOException e) {
				e.printStackTrace();
				stringBuilder.append("");
			}
		} else if (!f.exists()) {
			return null;
		}
		return stringBuilder.toString();
	}

	/*
	 * 解析歌曲时间处理类
	 */
	public int TimeStr(String timeStr) {
		timeStr = timeStr.replace(":", ".").trim();
		timeStr = timeStr.replace(".", "@").trim();
		String timeData[] = timeStr.split("@");
//		Log.e("LrcProgress", "LrcProgress(分离的时间数组长度):" + timeData.length);
		// 分离出分、秒并转换为整型
		int minute = 0, second = 0, millisecond = 0;
		switch (timeData.length) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			if (checkStr(timeData[0])) {
				minute = Integer.parseInt(timeData[0]);
			}
			if (checkStr(timeData[1])) { // 严格验证 时间为正确的时间值
				second = Integer.parseInt(timeData[1]);
			}
			if (checkStr(timeData[2])) { // 严格验证 时间为正确的时间值
				millisecond = Integer.parseInt(timeData[2]);
			}
			break;

		default:
			break;
		}

		// 计算上一行与下一行的时间转换为毫秒数
		int currentTime = (minute * 60 + second) * 1000 + millisecond * 10;
		return currentTime;
	}

	/**
	 * 检查是否为合法的时间(数字)
	 */
	public boolean checkStr(String str) {
		boolean isNum = str.matches("[0-9]+");
		return isNum;
	}

	public List<LrcContent> getLrcContent() {
		return LrcList;
	}

	/**
	 * 获得歌词和时间并返回的类
	 */
	public class LrcContent {
		private String Lrc;
		private int Lrc_time;

		public String getLrc() {
			return Lrc;
		}

		public void setLrc(String lrc) {
			Lrc = lrc;
		}

		public int getLrc_time() {
			return Lrc_time;
		}

		public void setLrc_time(int lrc_time) {
			Lrc_time = lrc_time;
		}
	}
}
