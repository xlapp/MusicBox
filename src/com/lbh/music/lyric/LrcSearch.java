package com.lbh.music.lyric;

import java.util.ArrayList;
import java.util.List;

import com.lbh.music.activity.Music;
import com.lbh.music.activity.MusicList;
import com.lbh.music.activity.PreferenceService;
import com.lbh.music.service.MusicService;
import com.lbh.music.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.lbh.music.R;

/**
 * 歌词查找
 * 
 */
public class LrcSearch extends Activity {
	LinearLayout noshow;
	EditText lrcurledit;
	Button exitlrc, enterlrc;
	PreferenceService service;
	private List<Music> lists;
	boolean isview = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.lrcsearch);
		service = new PreferenceService(this);
		lists = MusicList.getMusicData(getApplicationContext());
		lrcurledit = (EditText) this.findViewById(R.id.lrcurledit);
		exitlrc = (Button) this.findViewById(R.id.exitlrc);
		enterlrc = (Button) this.findViewById(R.id.enterlrc);
		exitlrc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	@Override
	protected void onStart() {
		Log.v("LrcSearch", "LrcSearch: onStart() ");
		if (lists.size() > 0) {
			final Music m = lists.get(MusicService._id);
			String url = m.getUrl(); // 获取歌曲路劲
			for (int i = url.length(); i >= 0; i--) {
				if (!url.substring(url.length() - 1, url.length()).equals("/")) {
					url = url.substring(0, url.length() - 1);
				}
			}
			Log.e("LrcSearch", "LrcSearch: 歌曲路劲:" + url);
			lrcurledit.setText(url);
			enterlrc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					service.savelrcurl(lrcurledit.getText().toString());
					if (Utils.networkState(LrcSearch.this)) { // 有网络
						new Thread(new Runnable() {

							@Override
							public void run() {
								SearchLRC searchLRC = new SearchLRC(m);
								searchLRC.fetchLyric(); // 从网络下载歌词
									sendBroadcast(new Intent(
											"com.cn.lrcDownFinish"));
								// for (int i = 0; i < fetchLyric.size(); i++) {
								// Log.v("LrcSearch", "LrcSearch: "
								// + fetchLyric.get(i).toString());
								// }
							}
						}).start();
						Toast.makeText(getApplicationContext(),
								"正在努力下载歌词，请稍后...", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(),
								"网络错误，请连接网络后重试！", Toast.LENGTH_SHORT).show();

					}
					finish();
				}
			});
		}
		super.onStart();
	}
}
