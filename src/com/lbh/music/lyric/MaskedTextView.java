package com.lbh.music.lyric;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;

public class MaskedTextView extends View{
    
    private Paint txtPaint;
    private Shader shader;
    private float dx=50;
    private long lastTime = System.currentTimeMillis();
    private boolean start = false;
    
    
    private void init(){
        txtPaint = new Paint();
        txtPaint.setColor(Color.GRAY);
        txtPaint.setAntiAlias(true);
        txtPaint.setTextSize(36);
        shader = new LinearGradient(0, 0, 200, 0, 
               new int[]{Color.argb(255, 120, 120, 120), Color.argb(255, 120, 120, 120), Color.RED}, new float[]{0, 0.7f, 1}, TileMode.MIRROR);
        txtPaint.setShader(shader);
    }
    
    public MaskedTextView(Context context) {
        
        super(context);
        init();
    }
    
    public MaskedTextView(Context context, AttributeSet attrs){
        super(context, attrs);
        init();
    }
    
    public void setStart(boolean start) {
        this.start = start;
        invalidate();
    }
    
    
    @Override
    protected void onDraw(Canvas canvas) {
        long now = System.currentTimeMillis();
        float elapsed = (now - lastTime)/4.5f;
        dx += elapsed;
        Matrix matrix = new Matrix();
        if(start){
            matrix.setTranslate(dx, 0);
            invalidate();
        }else{
            matrix.setTranslate(0, 0);
        }

        shader.setLocalMatrix(matrix);
        canvas.drawText("哥已不在江湖多年", 0, 35, txtPaint);
        lastTime = now;
    }
    
}