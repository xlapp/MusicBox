package com.lbh.music.service;

import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * wegit控件 下一首歌 后台服务
 * 
 */
public class WidgetNextService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		Log.v("WidgetNextService", "WidgetNextService: onStart()");
		String a = "no";
		try {
			a = intent.getStringExtra("play");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!a.equals("no")) {
			MusicNum.putplay(5);
			MusicNum.putisok(true);
			Intent intent2 = new Intent(WidgetNextService.this,
					MusicService.class);
			startService(intent2);
		}
		super.onStart(intent, startId);
	}
	// }

}