package com.lbh.music.service;

import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * wegit控件 上一首歌 后台服务
 * 
 */
public class WidgetFrontService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.v("WidgetFrontService", "WidgetFrontService: onCreate()");
		super.onCreate();
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		String a = "no";
		try {
			a = intent.getStringExtra("play");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!a.equals("no")) {
			MusicNum.putplay(7);
			MusicNum.putisok(true);
			Intent intent2 = new Intent(WidgetFrontService.this,
					MusicService.class);
			startService(intent2);
		}
		super.onStart(intent, startId);
	}
	// }

}