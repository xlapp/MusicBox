package com.lbh.music.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {

	/**
	 * 
	 * @overview - Judge the phone's network is available
	 * @param -
	 * @return - true network is available, or false network is not available
	 */
	public static Boolean networkState(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			// Obtain all network connection information
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {// Search for each state has connected network
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}

		return false;
	}
}
