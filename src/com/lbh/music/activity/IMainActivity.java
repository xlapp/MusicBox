package com.lbh.music.activity;

import java.util.List;

import com.lbh.music.sava.Indexviewpager;
import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import com.lbh.music.R;

/**
 * 
 *歌曲
 */
public class IMainActivity extends Activity {
	private ListView listView;
	private Close close;
	private ImageButton musiclist_play, musiclist_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v("IMainActivity", "IMainActivity: onCreate()");
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		this.setContentView(R.layout.iactivity_main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.title_bar);
		listView = (ListView) this.findViewById(R.id.ilistview);
		close = new Close();
		IntentFilter filter22 = new IntentFilter("com.sleep.close");
		this.registerReceiver(close, filter22);
		List<Music> listMusic = MusicList.getMusicData(getApplicationContext());
		musiclist_play = (ImageButton) this.findViewById(R.id.musiclist_play);
		musiclist_back = (ImageButton) this.findViewById(R.id.musiclist_back);
		musiclist_play.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(IMainActivity.this,
						MusicActivity.class);
				startActivity(intent);

			}
		});
		musiclist_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();

			}
		});
		MusicAdapter adapter = new MusicAdapter(IMainActivity.this, listMusic);
		// layoutParams.height=800;

		// Index.mainindexback.setLayoutParams(layoutParams);
		listView.setAdapter(adapter);
		MusicList.getMusicData(this);
		listView.setSelection(Indexviewpager.getmainlistposition());
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent1 = new Intent(IMainActivity.this,
						MusicService.class);
				// intent1.putExtra("play",8);
				MusicNum.putplay(8);
				MusicNum.putisok(true);
				intent1.putExtra("_id", arg2);
				startService(intent1);

				Intent intent = new Intent(IMainActivity.this,
						MusicActivity.class);
				// intent.putExtra("id", arg2);
				startActivity(intent);
			}
		});
		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
			}

			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
				Indexviewpager.putmainlistposition(arg1);
				// LinearLayout.LayoutParams
				// layoutParams=(LinearLayout.LayoutParams)Index.mainindexback.getLayoutParams();

				// / if(arg1>0){
				// for(int a=0;a<=arg1;a++){
				// if(a>400){
				// a=400;}
				// / layoutParams.height=700-a*2;
				// Index.mainindexback.setLayoutParams(layoutParams);
				// }
				// layoutParams.height=300;
				//

				// }
				// else{
				// layoutParams.height=700;
				// //
				// Index.mainindexback.setLayoutParams(layoutParams);
				// }
				// int a=800-arg1*20;
				// if(a<=400){
				// a=400;
				// }
				// layoutParams.height=a;
				//
				// Index.mainindexback.setLayoutParams(layoutParams);
				// Log.i("arg1", String.valueOf(arg1)); //滚动到的位置
				// Log.i("arg2", String.valueOf(arg2));
				// Log.i("arg3", String.valueOf(arg3)); //list总数
			}
		});
	}

	@Override
	protected void onDestroy() {
		listView.setAdapter(null);
		this.unregisterReceiver(close);
		super.onDestroy();
	}

	public class Close extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();

		}
	}
}
