package com.lbh.music.activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.lbh.music.R;

/**
 * 歌列表
 */
public class List extends Activity {
	private Close close;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v("List", "List onCreate()");
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.list);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.title_bar);
		ImageButton equze_back = (ImageButton) this
				.findViewById(R.id.musiclist_back);
		ImageButton musiclist_play = (ImageButton) this
				.findViewById(R.id.musiclist_play);
		TextView titlename = (TextView) this.findViewById(R.id.titlename);
		titlename.setText("列表");

		close = new Close(); // 注册退出程序广播
		IntentFilter filter22 = new IntentFilter("com.sleep.close");
		this.registerReceiver(close, filter22);

		// 播放监听
		musiclist_play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(List.this, MusicActivity.class);
				startActivity(intent);
			}
		});
		// 返回键 监听事件
		equze_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		ListView list = (ListView) findViewById(R.id.ListView01);

		ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
		String dianpuname[] = { "专辑列表", "收藏列表", "最近播放", "音乐查找" };
		String miaoshu[] = { "列举所有专辑", "我收藏过的歌曲", "播放器最近播放过的音乐", "查找本地音乐" };
		for (int i = 0; i <= 3; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("em_name", dianpuname[i]);
			map.put("em_time", miaoshu[i]);
			listItem.add(map);
		}
		SimpleAdapter listItemAdapter = new SimpleAdapter(this, listItem,// 数据源
				R.layout.list_items,// ListItem的XML实现
				new String[] { "em_name", "em_time" }, new int[] {
						R.id.em_name, R.id.em_time });
		list.setAdapter(listItemAdapter);

		// listView 选项监听事件
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (arg2 == 0) {
					Intent intent = new Intent(List.this, AlbumsActivity.class);
					startActivity(intent);
				}
				if (arg2 == 1) {
					Intent intent = new Intent(List.this,
							RecentlyActivity.class);
					startActivity(intent);
				}
				if (arg2 == 2) {
					Intent intent = new Intent(List.this, NowActivity.class);
					startActivity(intent);
				}
				if (arg2 == 3) {
					Intent intent = new Intent(List.this, Search.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// 销毁注册广播
		unregisterReceiver(close);
	}

	public class Close extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	}
}
