package com.lbh.music.activity;

import java.util.Map;

import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;
import com.lbh.music.ui.VerticalSeekBar;
import com.lbh.music.ui.VerticalSeekBar.OnValueChangedListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.lbh.music.R;

/**
 *声音管理
 */
public class Equize extends Activity {
	private Close close;
	private int maxVolume, currentVolume;// 最大音量
	private AudioManager audioManager;// 音量管理者
	short minEQLevel = -1000, maxEQLevel = 1000, band;
	static String zidai[] = { "普通", "古典", "舞曲", "民谣", "重金属", "嘻哈", "爵士", "流行",
			"摇滚" };
	private Audiachange receiver322;
	TextView equzechoose;
	LinearLayout equzequxianlin;
	VerticalSeekBar bar1, bar2, bar3, bar4, bar5, bar6;
	boolean choose = false;
	private ImageView equzesave, equzechoosebut;
	private static final String TAG = "AudioFxActivity";
	static PreferenceService service;
	int prog[][] = { { 0, 0, 0, 0, 0 }, { 416, 200, -166, 332, 332 },
			{ 500, 0, 166, 332, 83 }, { 200, 0, 0, 166, -83 },
			{ 332, 83, 747, 200, 0 }, { 416, 200, 0, 83, 200 },
			{ 332, 166, -166, 166, 416 }, { -83, 166, 416, 83, -166 },
			{ 416, 250, -83, 250, 416 } };
	int b0, b1, b2, b3, b4;
	int b[] = { b0, b1, b2, b3, b4 };
	int w, h;
	DisplayMetrics dm;
	DrawView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.mainequze);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.title_bar);
		ImageButton equze_back = (ImageButton) this
				.findViewById(R.id.musiclist_back);
		ImageButton musiclist_play = (ImageButton) this
				.findViewById(R.id.musiclist_play);
		TextView titlename = (TextView) this.findViewById(R.id.titlename);
		titlename.setText("均衡器");
		musiclist_play.setVisibility(View.GONE);

		equze_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		equzesave = (ImageView) this.findViewById(R.id.equzesave);

		equzechoosebut = (ImageView) this.findViewById(R.id.equzechoose);
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);// 获得最大音量
		currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 获得当前音量

		service = new PreferenceService(this);

		equzechoose = (TextView) this.findViewById(R.id.equzechoosetext);
		equzequxianlin = (LinearLayout) this.findViewById(R.id.equzequxianlin);
		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		w = dm.widthPixels;
		view = new DrawView(this);
		view.setMinimumHeight(500);
		view.setMinimumWidth(300);
		view.invalidate();
		equzequxianlin.addView(view);
		close = new Close();
		IntentFilter filter22 = new IntentFilter("com.sleep.close");
		this.registerReceiver(close, filter22);
		equzesave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		receiver322 = new Audiachange();
		IntentFilter filter322 = new IntentFilter("cn.com.karl.progress");
		this.registerReceiver(receiver322, filter322);

		bar1 = (VerticalSeekBar) findViewById(R.id.barvolum);
		bar2 = (VerticalSeekBar) findViewById(R.id.bar2);
		bar3 = (VerticalSeekBar) findViewById(R.id.bar3);
		bar4 = (VerticalSeekBar) findViewById(R.id.bar4);
		bar5 = (VerticalSeekBar) findViewById(R.id.bar5);
		bar6 = (VerticalSeekBar) findViewById(R.id.bar6);
		Map<String, String> params00 = service.getPreferences00();
		Map<String, String> params11 = service.getPreferences01();
		Map<String, String> params22 = service.getPreferences02();
		Map<String, String> params33 = service.getPreferences03();
		Map<String, String> params44 = service.getPreferences04();

		int params[] = { Integer.valueOf(params00.get("progress0")),
				Integer.valueOf(params11.get("progress1")),
				Integer.valueOf(params22.get("progress2")),
				Integer.valueOf(params33.get("progress3")),
				Integer.valueOf(params44.get("progress4")) };
		final VerticalSeekBar bar[] = { bar2, bar3, bar4, bar5, bar6 };

		for (int i = 0; i <= 8; i++) {
			final int n = i;
			if (params[0] == (prog[n][0] + maxEQLevel)
					&& params[1] == (prog[n][1] + maxEQLevel)
					&& params[2] == (prog[n][2] + maxEQLevel)
					&& params[3] == (prog[n][3] + maxEQLevel)
					&& params[4] == (prog[n][4] + maxEQLevel)) {
				equzechoose.setText(zidai[n]);
				break;
			} else {
				equzechoose.setText("手动");
			}
		}
		bar1.SetOnValueChangedListener(new OnValueChangedListener() {
			@Override
			public void VauleChanged(int val) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, val,
						AudioManager.FLAG_ALLOW_RINGER_MODES);
			}
		});
		bar1.SetMaxMin(0, maxVolume);
		bar1.SethandlePos(currentVolume, false);

		for (int i = 0; i < 5; i++) {
			final int n = i;
			bar[i].SetMaxMin(0, 2000);
			bar[i].SethandlePos(params[n], false);
			b[n] = params[n] * 3 / 50;
			view.invalidate();
			bar[n].SetOnValueChangedListener(new OnValueChangedListener() {
				@Override
				public void VauleChanged(int val) {
					if (MusicService.player != null) {
						Intent intent = new Intent(Equize.this,
								MusicService.class);
						intent.putExtra("progress", val);
						intent.putExtra("number", n);
						MusicNum.putplay(14);
						MusicNum.putisok(true);
						startService(intent);
					}
					if (n == 0) {
						b[0] = val * 3 / 50;
						service.band0(TAG, val);
					}
					if (n == 1) {
						b[1] = val * 3 / 50;
						service.band1(TAG, val);
					}
					if (n == 2) {
						b[2] = val * 3 / 50;
						service.band2(TAG, val);
					}
					if (n == 3) {
						b[3] = val * 3 / 50;
						service.band3(TAG, val);
					}
					if (n == 4) {
						b[4] = val * 3 / 50;
						service.band4(TAG, val);
					}
					if (!choose) {
						equzechoose.setText("手动");
					}
					view.invalidate();
				}
			});
		}

		equzechoosebut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				choose = true;
				new AlertDialog.Builder(Equize.this)
						.setItems(zidai, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								equzechoose.setText(zidai[arg1]);
								for (int i = 0; i <= arg1; i++) {
									final int n = i;
									for (int j = 0; j <= 4; j++) {
										final int m = j;
										bar[m].SethandlePos(prog[n][m]
												+ maxEQLevel, true);
									}
								}
							}
						}).create().show();
			}
		});
	}

	public class DrawView extends View {
		public DrawView(Context context) {
			super(context);
		}

		@SuppressLint("DrawAllocation")
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			// 创建画笔
			Paint p = new Paint();
			p.setShadowLayer(12, 12, 12, Color.parseColor("#f68350"));
			p.setColor(Color.parseColor("#f99200"));// 设置绿色
			canvas.drawLine(14, 120 - b[0], w / 4, 120 - b[1], p);// 斜线
			canvas.drawLine(w / 4, 120 - b[1], w / 2, 120 - b[2], p);// 斜线
			canvas.drawLine(w / 2, 120 - b[2], w * 3 / 4, 120 - b[3], p);// 斜线
			canvas.drawLine(w * 3 / 4, 120 - b[3], w - 14, 120 - b[4], p);// 斜线

		}
	}

	@Override
	protected void onDestroy() {
		this.unregisterReceiver(receiver322);
		this.unregisterReceiver(close);
		super.onDestroy();
	}

	public class Audiachange extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			currentVolume = audioManager
					.getStreamVolume(AudioManager.STREAM_MUSIC);// 获得当前音量
			bar1.SethandlePos(currentVolume, false);
		}
	}

	public class Close extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	}
}
