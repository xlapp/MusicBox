package com.lbh.music.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lbh.music.lyric.LyricActivity;
import com.lbh.music.lyric.NoActivity;
import com.lbh.music.sava.Indexviewpager;
import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;
import com.lbh.music.service.SleepService;
import com.lbh.music.service.VolumService;
import com.lbh.music.service.WidgetFrontService;
import com.lbh.music.service.WidgetNextService;
import com.lbh.music.service.WidgetPlayService;
import com.lbh.music.utils.ToTime;

import android.annotation.SuppressLint;
import android.app.ActivityGroup;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.lbh.music.R;

/**
 * 
 * 播放器页面(播放音乐)
 */
public class MusicActivity extends ActivityGroup {
	String KEY[] = { "remember4", "remember5", "remember9", "remember10",
			"remember12", "remember13", "nowplay", "prefsname" };
	private SharedPreferences mSettings = null;
	private TextView textName, textSinger, allmusic, currentmusic, textEndTime;
	static TextView textStartTime;
	private ImageButton imageBtnRewind;
	// public static ImageButton music_equze;

	private ImageButton imageBtnForward;
	public static ImageButton imageBtnRandom;
	private ImageButton play_back;
	public ImageButton imageBtnPlay;
	// 显示动画的组件

	private AudioManager am; // 音频管理引用，提供对音频的控制
	RelativeLayout ll_player_voice; // 音量控制面板布局
	int currentVolume; // 当前音量
	int maxVolume; // 最大音量
	public static ImageButton music_voice; // 显示音量控制面板的按钮
	SeekBar sb_player_voice; // 控制音量大小

	// 音量面板显示和隐藏动画
	private Animation showVoicePanelAnimation;
	private Animation hiddenVoicePanelAnimation;

	private ImageView imgDance;

	// Frame动画

	private AnimationDrawable animDance;

	ToTime totime;
	String time, musicna;
	static SeekBar seekBar1;
	private List<Music> lists;
	SharedPreferences.Editor saveput;
	SharedPreferences saveget;
	boolean exist;
	static int currentId = 0;
	private MyProgressBroadCastReceiver receiver;
	private MusicPlay6er receiver9;
	private Close close;
	int current5, progress;
	static ImageView imagebg;
	ImageView page_icon;
	ImageView imageb;
	public ImageView love;
	static PreferenceService service;
	private Animation myAnimation, myAnimationx;
	private int page[] = { R.drawable.page_icon_left, R.drawable.page_icon_mid,
			R.drawable.page_icon_right };
	private ViewPager viewPager;
	private ArrayList<View> pageViews;
	private Bitmap bitmap = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v("MusicActivity", "MusicActivity : onCreate()");
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.music);
		lists = MusicList.getMusicData(this);
		service = new PreferenceService(this);
		mSettings = getSharedPreferences(KEY[7], Context.MODE_PRIVATE);
		saveget = getSharedPreferences("shoucang", 0);
		saveput = getSharedPreferences("shoucang", 0).edit();
		if (MusicNum.getbtn(8)) {
			getWindow()
					.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
		totime = new ToTime();
		// 找到控件
		findviews();
		// 注册广播3个
		register();

		// 按钮点击监听
		buttonclick();

		InItView();
		viewPager.setAdapter(new myPagerView());
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				setSelector(arg0);
			}

			@Override
			public void onPageScrolled(int paramInt, float paramFloat, int arg2) {
				Indexviewpager.putmusic(paramInt);
				if (paramInt == 0 && arg2 != 0) {
					if (paramFloat < 0.3) {
						paramFloat = 0.3f;
					}
					imagebg.setAlpha(paramFloat);
				}

				if (paramInt == 1 && arg2 != 0) {
					if (paramFloat < 0) {
						paramFloat = 0f;
					}
					if (paramFloat > 0.7) {
						paramFloat = 0.7f;
					}
					imagebg.setAlpha(1 - paramFloat);
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		// 拖动进度条

		// 获得背景（6个图片形成的动画）

		// this.animDance = (AnimationDrawable) this.seekBar1.getThumb();
		// this.animDance.start();

		seekBar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			public void onStopTrackingTouch(SeekBar seekBar) {
				// 结束拖动
				if (lists.size() > 0) {
					if (MusicService.player != null) {
						MusicService.player.seekTo(seekBar.getProgress()
								* MusicService.player.getDuration() / 1000);
					} else {
						Music m = lists.get(currentId);
						service.save3(
								textStartTime.getText().toString(),
								Integer.valueOf((int) (seekBar1.getProgress()
										* m.getTime() / 1000)));
					}

					seekBar1.setProgress(seekBar.getProgress());
				} else {
					Toast.makeText(getApplicationContext(), "列表为空，定位失败！", 1)
							.show();
				}
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
				// 开始拖动
			}

			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// 正在拖动
				if (lists.size() > 0) {
					Music m = lists.get(currentId);
					seekBar1.setProgress(seekBar.getProgress());
					textStartTime.setText(totime.toTime((int) (progress
							* m.getTime() / 1000)));
				} else {
					Log.i("ds", "列表为空，定位失败！");
				}
			}
		});
		run = true;
		handler.postDelayed(task, 10);
	}

	private void setbg() {
		SharedPreferences localSharedPreferences = getSharedPreferences(
				"music", 0);
		Uri uri = Uri.parse(localSharedPreferences.getString("background", ""));
		ContentResolver contentResolver = this.getContentResolver();

		try {
			bitmap = BitmapFactory.decodeStream(contentResolver
					.openInputStream(uri));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (localSharedPreferences.getString("background", "").equals("")) {
			imagebg.setBackgroundResource(R.drawable.bg_media_library);
			imagebg.setVisibility(View.VISIBLE);
			imageb.setVisibility(View.INVISIBLE);

		} else {
			imagebg.setImageBitmap(bitmap);
			imagebg.setVisibility(View.VISIBLE);
			imagebg.startAnimation(myAnimation);
			imageb.startAnimation(myAnimationx);
			imageb.setVisibility(View.INVISIBLE);
		}

		// UserService uService=new UserService(MusicActivity.this);
		// boolean flag=uService.login(String.valueOf(MusicService._id));
		// if(flag){
		// love.setImageResource(R.drawable.menu_add_to_favorite_n);
		// }else{
		// love.setImageResource(R.drawable.menu_add_to_favorite_d); //变暗
		// }
	}

	public void setSelector(int id) {
		for (int i = 0; i < page.length; i++) {
			if (id == i) {
				// 设置底部图片
				page_icon.setImageResource(page[i]);
				viewPager.setCurrentItem(i);
			}
		}
	}

	void InItView() {
		pageViews = new ArrayList<View>();

		View view01 = getLocalActivityManager().startActivity("activity01",
				new Intent(this, MainActivity.class)).getDecorView();

		View view02 = getLocalActivityManager().startActivity("activity02",
				new Intent(this, NoActivity.class)).getDecorView();

		View view03 = getLocalActivityManager().startActivity("activity03",
				new Intent(this, LyricActivity.class)).getDecorView();
		pageViews.add(view01);
		pageViews.add(view02);
		pageViews.add(view03);
	}

	private void buttonclick() {
		imageBtnRewind.setOnClickListener(new MyListener());
		imageBtnPlay.setOnClickListener(new MyListener());
		imageBtnForward.setOnClickListener(new MyListener());
		imageBtnRandom.setOnClickListener(new MyListener());
		play_back.setOnClickListener(new MyListener());
		// music_equze.setOnClickListener(new MyListener());
		music_voice.setOnClickListener(new MyListener());
	}

	private void register() {
		receiver = new MyProgressBroadCastReceiver();
		IntentFilter filter = new IntentFilter("cn.com.karl.progress");
		this.registerReceiver(receiver, filter);

		receiver9 = new MusicPlay6er();
		IntentFilter filter9 = new IntentFilter("com.cn.musicserviceplayer");
		this.registerReceiver(receiver9, filter9);

		close = new Close();
		IntentFilter filter22 = new IntentFilter("com.sleep.close");
		this.registerReceiver(close, filter22);

	}

	private void findviews() {
		viewPager = (ViewPager) findViewById(R.id.pager);
		myAnimation = AnimationUtils.loadAnimation(this, 0x7f040001);
		myAnimationx = AnimationUtils.loadAnimation(this, 0x7f040000);
		textName = (TextView) this.findViewById(R.id.music_nameq);
		textSinger = (TextView) this.findViewById(R.id.music_singerq);
		textStartTime = (TextView) this.findViewById(R.id.music_start_time);
		textEndTime = (TextView) this.findViewById(R.id.music_end_time);
		currentmusic = (TextView) this.findViewById(R.id.currentmusic);
		allmusic = (TextView) this.findViewById(R.id.allmusic);
		seekBar1 = (SeekBar) this.findViewById(R.id.music_seekBar);
		imageBtnRewind = (ImageButton) this.findViewById(R.id.music_rewind);
		imageBtnPlay = (ImageButton) this.findViewById(R.id.music_play);
		// music_equze = (ImageButton) this.findViewById(R.id.music_equze);
		music_voice = (ImageButton) this.findViewById(R.id.music_voice);
		play_back = (ImageButton) this.findViewById(R.id.play_back);
		imageBtnForward = (ImageButton) this.findViewById(R.id.music_foward);
		imageBtnRandom = (ImageButton) this.findViewById(R.id.music_random);
		love = (ImageView) this.findViewById(R.id.love);
		imagebg = (ImageView) this.findViewById(R.id.imageb);
		imageb = (ImageView) this.findViewById(R.id.imagebg);
		page_icon = (ImageView) this.findViewById(R.id.page_icon);

		// mydbHelper = new MyDatabaseHelper(getBaseContext());//创建数据库辅助类
		// db = mydbHelper.getReadableDatabase();//获取SQLite数据库
		allmusic.setText(String.valueOf(lists.size()));

		music_voice = (ImageButton) this.findViewById(R.id.music_voice);
		ll_player_voice = (RelativeLayout) findViewById(R.id.ll_player_voice);
		sb_player_voice = (SeekBar) findViewById(R.id.sb_player_voice);
		sb_player_voice.setOnSeekBarChangeListener(new SeekBarChangeListener());

	}

	// 控制声音的SeekBar改变事件监听
	private class SeekBarChangeListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			switch (seekBar.getId()) {

			case R.id.sb_player_voice:
				// 设置音量
				am.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
				Log.v("MusicActivity am--->", "am--->" + progress);
				break;
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {

		}

	}

	@Override
	protected void onStart() {
		// 得到上次保存的歌曲
		Map<String, String> params = service.getPreferences();
		Map<String, String> params3 = service.getPreferences3();
		if (MusicService.player == null) {
			int ida = Integer.valueOf(params.get("currentId"));
			progress = Integer.valueOf(params3.get("progress"));
			String time = params3.get("time");
			try {
				Music m = lists.get(ida);
				// if(MusicNum.getbtn(9)){
				seekBar1.setProgress((int) (progress * 1000 / m.getTime()));
				textStartTime.setText(time);
				// }
				if (lists.size() > 0) {
					currentmusic.setText(String.valueOf(ida + 1));
				} else {
					currentmusic.setText("0");
				}
				textName.setText(m.getTitle());
				if (m.getSinger().equals("未知艺术家")) {
					textSinger.setText("music");
				} else {
					textSinger.setText(m.getSinger());
				}
				textEndTime.setText(totime.toTime((int) m.getTime()));
				currentId = ida;
				toasta();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (MusicService.player.isPlaying()) {
			Music m = lists.get(MusicService._id);
			textEndTime.setText(totime.toTime((int) m.getTime()));
			textName.setText(m.getTitle());
			if (m.getSinger().equals("未知艺术家")) {
				textSinger.setText("music");
			} else {
				textSinger.setText(m.getSinger());
			}
			if (lists.size() > 0)
				currentmusic.setText(String.valueOf(MusicService._id + 1));
			else
				currentmusic.setText("0");

			imageBtnPlay.setBackgroundResource(R.drawable.pause1);
		} else {
			Music m = lists.get(MusicService._id);
			seekBar1.setProgress((int) (MusicService.player
					.getCurrentPosition() * 1000 / MusicService.player
					.getDuration()));
			textStartTime.setText(totime.toTime(MusicService.player
					.getCurrentPosition()));
			if (lists.size() > 0)
				currentmusic.setText(String.valueOf(MusicService._id));
			else
				currentmusic.setText("0");
			textName.setText(m.getTitle());
			if (m.getSinger().equals("未知艺术家")) {
				textSinger.setText("music");
			} else {
				textSinger.setText(m.getSinger());
			}
			textEndTime.setText(totime.toTime((int) MusicService.player
					.getDuration()));
			currentId = MusicService._id;
			toasta();
		}
		// }
		//
		super.onStart();
	}

	@Override
	protected void onResume() {
		if (MusicNum.getbtn(20)) {
			setbg();
			MusicNum.putusbtn(20, false);
		}

		setSelector(Indexviewpager.getputmusic());
		if (Indexviewpager.getputmusic() == 0
				|| Indexviewpager.getputmusic() == 2) {
			imagebg.setAlpha(0.3f);
			imageb.setAlpha(0.3f);
		}
		viewPager.setCurrentItem(Indexviewpager.getputmusic());

		exist = getexist(String.valueOf(currentId));
		if (exist) {
			love.setImageResource(R.drawable.menu_add_to_favorite_n);
		} else {
			love.setImageResource(R.drawable.menu_add_to_favorite_d);
		}
		love.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (lists.size() > currentId) {
					if (!exist) {
						addtosave(String.valueOf(currentId));
						love.setImageResource(R.drawable.menu_add_to_favorite_n); // 变白
						Toast.makeText(getApplicationContext(), "已添加到收藏列表",
								Toast.LENGTH_SHORT).show();
						exist = true;
					} else if (exist) {
						try {
							delfromsave(String.valueOf(currentId));
							love.setImageResource(R.drawable.menu_add_to_favorite_d); // 变暗
							Toast.makeText(getApplicationContext(), "已从收藏列表移除",
									Toast.LENGTH_SHORT).show();
							exist = false;
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(), "抱歉，删除失败！",
									Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					Toast.makeText(getApplicationContext(), "抱歉，操作失败！",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		if (MusicNum.getbtn(11) && !MusicNum.getbtn(12)) {
			imageBtnRandom.setBackgroundResource(R.drawable.play_loop_specok);
		} else if (MusicNum.getbtn(12) && !MusicNum.getbtn(11)) {
			imageBtnRandom.setBackgroundResource(R.drawable.play_random_selok);
		} else if (!MusicNum.getbtn(12) && !MusicNum.getbtn(11)) {
			imageBtnRandom.setBackgroundResource(R.drawable.play_loop_selok);
		} else {
		}

		try {
			showVoicePanelAnimation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.push_up_in);
			hiddenVoicePanelAnimation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.push_up_out);
		} catch (Exception e) {
			// TODO: handle exception
		}

		// 获得系统音频管理服务对象
		am = (AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE);
		currentVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		sb_player_voice.setMax(maxVolume);
		sb_player_voice.setProgress(currentVolume);
		am.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);

		super.onResume();
	}

	private long count = 0;
	private boolean run = false;

	private Handler handler = new Handler();

	private Runnable task = new Runnable() {

		public void run() {
			if (run) {
				handler.postDelayed(this, 10);
				count++;
			}
			if (count > 0) {
				setbg();
				run = false;
			}
		}
	};

	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.v("MusicActivity", "MusicActivity: onDestroy()");
		this.unregisterReceiver(receiver);
		this.unregisterReceiver(close);
		this.unregisterReceiver(receiver9);
		// 回收图片
		if (this.bitmap != null && !this.bitmap.isRecycled()) {
			bitmap.recycle();
			bitmap = null;
		}
		System.gc();
	}

	public class Close extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	}

	// 进度条处理事件（不修改）
	public class MyProgressBroadCastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (MusicService.player != null) {
				if (MusicService.player.isPlaying()) {
					currentId = MusicService._id;
				}
				long position = MusicService.player.getCurrentPosition();
				long total = MusicService.player.getDuration();
				textEndTime.setText(String.valueOf(totime.toTime((int) total)));
				if (seekBar1.getProgress() < 1) {
					textEndTime.setText(String.valueOf(totime
							.toTime((int) total)));
				}
				if (position > total) {
					position = total;
				} else {
					textStartTime.setText(totime.toTime((int) position));
					seekBar1.setProgress((int) (position * 1000 / total));
					seekBar1.invalidate();
				}
			}
		}
	}

	// prepare完成后
	public class MusicPlay6er extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			currentId = MusicService._id;
			Music m = lists.get(currentId);
			textEndTime.setText(totime.toTime((int) m.getTime()));
			textName.setText(m.getTitle());
			if (m.getSinger().equals("未知艺术家")) {
				textSinger.setText("music");
			} else {
				textSinger.setText(m.getSinger());
			}
			if (lists.size() > 0)
				currentmusic.setText(String.valueOf(currentId + 1));
			else
				currentmusic.setText("0");
			imageBtnPlay.setBackgroundResource(R.drawable.pause1);
			toasta();
			exist = getexist(String.valueOf(currentId));
			if (exist) {
				love.setImageResource(R.drawable.menu_add_to_favorite_n);
			} else {
				love.setImageResource(R.drawable.menu_add_to_favorite_d);
			}
			// Music m1=lists.get(currentId);
			// Bitmap bitmap = MediaUtil.getArtwork(context,currentId,currentId,
			// true, true);
			// imageb.setImageBitmap(bitmap);
			// imagebg.setImageBitmap(bitmap);

		}
	}

	// 各种按键监听器（不修改）
	private class MyListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			Map<String, String> params4 = service.getPreferences4();
			current5 = Integer.valueOf(params4.get("musicid"));
			if (v == imageBtnRewind) { // 上一首
				MusicNum.putint(4);
				Intent intent = new Intent(MusicActivity.this,
						MusicService.class);
				MusicNum.putplay(7);
				MusicNum.putisok(true);
				startService(intent);
			} else if (v == imageBtnPlay) {
				// 正在播放
				if (MusicService.player != null && MusicService.nowplay) {
					imageBtnPlay.setBackgroundResource(R.drawable.play1);
				} else {
					if (lists.size() > 0)
						imageBtnPlay.setBackgroundResource(R.drawable.pause1);
				}
				Intent intent = new Intent(MusicActivity.this,
						MusicService.class);
				MusicNum.putplay(3);
				MusicNum.putisok(true);
				startService(intent);

			} else if (v == imageBtnForward) {
				MusicNum.putint(3);
				Intent intent = new Intent(MusicActivity.this,
						MusicService.class);
				MusicNum.putplay(5);
				MusicNum.putisok(true);
				startService(intent);
				// 下一首
			} else if (v == imageBtnRandom) {
				// 随机播放
				if (MusicNum.getbtn(12) == true && MusicNum.getbtn(11) == false) { // 点了随机变循环
					imageBtnRandom
							.setBackgroundResource(R.drawable.play_loop_selok); // xunhuan
					MusicNum.putusbtn(11, false);
					MusicNum.putusbtn(12, false);
					saveRemember13(false);
					saveRemember12(false);
				} else if (MusicNum.getbtn(11) == true
						&& MusicNum.getbtn(12) == false) { // 点了单曲变随机
					imageBtnRandom
							.setBackgroundResource(R.drawable.play_random_selok);// suiji
					MusicNum.putusbtn(11, false);
					MusicNum.putusbtn(12, true);

					saveRemember13(true);
					saveRemember12(false);
				} else if (!MusicNum.getbtn(11) && !MusicNum.getbtn(12)) { // 点了循环变单曲
					imageBtnRandom
							.setBackgroundResource(R.drawable.play_loop_specok);// danqu
					MusicNum.putusbtn(11, true);
					MusicNum.putusbtn(12, false);
					saveRemember13(false);
					saveRemember12(true);
				}

			} else if (v == music_voice) {
				// 声音调节
				// Intent intent = new Intent(MusicActivity.this, Equize.class);
				// startActivity(intent);
				voicePanelAnimation();
			} else if (v == play_back) {
				finish();
			}
		}
	}

	// 控制显示音量控制面板的动画
	public void voicePanelAnimation() {
		try {
			if (ll_player_voice.getVisibility() == View.GONE) {
				ll_player_voice.startAnimation(showVoicePanelAnimation);
				ll_player_voice.setVisibility(View.VISIBLE);
			} else {
				ll_player_voice.startAnimation(hiddenVoicePanelAnimation);
				ll_player_voice.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	// 记住随机循环（不修改）
	private void saveRemember12(boolean remember12) {
		Editor editor = mSettings.edit();// 获取编辑器
		editor.putBoolean(KEY[4], remember12);
		editor.commit();
	} // 获取保存的用户名

	// 记住循环和随机是否开启
	private void saveRemember13(boolean remember13) {
		Editor editor = mSettings.edit();// 获取编辑器
		editor.putBoolean(KEY[5], remember13);
		editor.commit();
	} // 获取保存的用户名

	public void toasta() {
		Music m = lists.get(MusicService._id);
		String Name = m.getName();
		service.save(Name, Integer.valueOf(MusicService._id));
	}

	// 单曲循环和随机播放图标显示和隐藏（不修改）
	class myPagerView extends PagerAdapter {
		// 显示数目
		@Override
		public int getCount() {
			return pageViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public int getItemPosition(Object object) {
			return super.getItemPosition(object);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(pageViews.get(arg1));
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(pageViews.get(arg1));
			return pageViews.get(arg1);
		}
	}

	/**
	 * 关闭后台服务
	 */
	public void stopService() {
		Log.e("Index Activity", "Index: stopService()");
		stopService(new Intent(MusicActivity.this, MusicService.class));
		stopService(new Intent(MusicActivity.this, SleepService.class));
		stopService(new Intent(MusicActivity.this, VolumService.class));
		stopService(new Intent(MusicActivity.this, WidgetNextService.class));
		stopService(new Intent(MusicActivity.this, WidgetFrontService.class));
		stopService(new Intent(MusicActivity.this, WidgetPlayService.class));
	}

	// 菜单Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// 菜单Menu 选项事件
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.exit) { // 发送退出程序的广播
			Intent intent = new Intent("com.sleep.close");
			sendBroadcast(intent);
			stopService();
			finish();
		}
		// if (item.getItemId() == R.id.about) { // 关于
		// Intent intent1 = new Intent(MusicActivity.this, About.class);
		// startActivity(intent1);
		// }

		if (item.getItemId() == R.id.refresh) {
			lists = MusicList.getMusicData(this);
			Toast.makeText(getApplicationContext(), "刷新成功！", 1).show();
		}

		// if (item.getItemId() == R.id.deletecurrent) { // 删除当前歌曲
		// if (MusicService.nowplay) {
		// File f = new File(lists.get(MusicService._id).getUrl());
		//
		// if (f.exists()) {
		// try {
		// f.delete();
		// if (MusicService.his > 0) {
		// MusicService.his -= 1;
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// MusicNum.putint(3);
		// Intent intent = new Intent(MusicActivity.this,
		// MusicService.class);
		// MusicNum.putplay(5);
		// MusicNum.putisok(true);
		// startService(intent);
		// Toast.makeText(getApplicationContext(), "删除成功！", 1).show();
		// }
		// } else {
		// Toast.makeText(getApplicationContext(), "抱歉,当前没有歌曲在播放！", 1)
		// .show();
		// }
		// }
		if (item.getItemId() == R.id.entersleep) { // 睡眠模式
			Intent intent1 = new Intent(MusicActivity.this, Sleep.class);
			startActivity(intent1);
		}
		if (item.getItemId() == R.id.menusearch) { // 本地歌曲查找
			Intent intent1 = new Intent(MusicActivity.this, Search.class);
			startActivity(intent1);
		}
		if (item.getItemId() == R.id.setting) { // 软件设置
			Intent intent1 = new Intent(MusicActivity.this, Setting.class);
			startActivity(intent1);
		}
		if (item.getItemId() == R.id.settingring) { // 设置 当前歌曲为来电铃声
			if (MusicService.player == null) {
				Toast.makeText(getApplicationContext(), "当前音乐为空！", 1).show();
			} else {
				try {
					Music m = lists.get(MusicService._id);
					String path = m.getUrl();
					setMyRingtone(path);
					Toast.makeText(getApplicationContext(), "设置铃声成功！", 1)
							.show();
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), "设置铃声失败！", 1)
							.show();
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	public void setMyRingtone(String path) {
		File file = new File(path);
		ContentValues values = new ContentValues();
		values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
		values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
		values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
		values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
		values.put(MediaStore.Audio.Media.IS_ALARM, false);
		values.put(MediaStore.Audio.Media.IS_MUSIC, false);
		Uri uri = MediaStore.Audio.Media.getContentUriForPath(file
				.getAbsolutePath());
		Uri newUri = this.getContentResolver().insert(uri, values);
		RingtoneManager.setActualDefaultRingtoneUri(this,
				RingtoneManager.TYPE_RINGTONE, newUri);
	}

	public boolean getexist(String id) {
		String all = saveget.getString("shoucang", "");
		String cuid = id;
		if (cuid.length() == 1) {
			cuid = cuid + "###";
		}
		if (cuid.length() == 2) {
			cuid = cuid + "##";
		}
		if (cuid.length() == 3) {
			cuid = cuid + "#";
		}
		if (all.contains(cuid)) {
			return true;
		} else {
			return false;
		}
	}

	public void addtosave(String id) {
		String all = saveget.getString("shoucang", "");
		String cuid = id;
		if (cuid.length() == 1) {
			cuid = cuid + "###";
		}
		if (cuid.length() == 2) {
			cuid = cuid + "##";
		}
		if (cuid.length() == 3) {
			cuid = cuid + "#";
		}
		all = all + cuid;
		saveput.putString("shoucang", all);
		saveput.commit();
	}

	public void delfromsave(String id) {
		String all = saveget.getString("shoucang", "");
		Log.i("shoucang", saveget.getString("shoucang", ""));
		String cuid = id;
		if (cuid.length() == 1) {
			cuid = cuid + "###";
		}
		if (cuid.length() == 2) {
			cuid = cuid + "##";
		}
		if (cuid.length() == 3) {
			cuid = cuid + "#";
		}
		all = all.replace(cuid, "");
		saveput.putString("shoucang", all);
		saveput.commit();
	}
}
