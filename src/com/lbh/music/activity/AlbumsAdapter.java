package com.lbh.music.activity;

import java.util.List;

import com.lbh.music.utils.ToTime;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.lbh.music.R;

/**
 * 歌曲信息适配器
 * 
 */
public class AlbumsAdapter extends BaseAdapter {
	ToTime time;
	private List<Music> mListMusic;
	private Context context;

	public AlbumsAdapter(Context context, List<Music> listMusic) {
		this.context = context;
		this.mListMusic = listMusic;
	}

	public void setListItem(List<Music> listMusic) {
		this.mListMusic = listMusic;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		Log.v("AlbumsAdapter", "AlbumsAdapter getCount():" + mListMusic.size());
		return mListMusic.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return mListMusic.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		time = new ToTime();

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.album_item, null);
		}
		Music m = mListMusic.get(position);
		// 音乐名
		TextView textMusicName = (TextView) convertView
				.findViewById(R.id.album_item_name);
		textMusicName.setText(m.getSinger());
		// 歌手
		TextView textMusicSinger = (TextView) convertView
				.findViewById(R.id.album_item_singer);
		textMusicSinger.setText(m.getName().subSequence(0,
				m.getName().length() - 4));
		// 持续时间
		TextView textMusicTime = (TextView) convertView
				.findViewById(R.id.album_item_time);
		textMusicTime.setText(time.toTime((int) m.getTime()));
		return convertView;
	}

}
