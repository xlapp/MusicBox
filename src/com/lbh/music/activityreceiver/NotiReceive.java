package com.lbh.music.activityreceiver;

import com.lbh.music.sava.MusicNum;
import com.lbh.music.service.MusicService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotiReceive extends BroadcastReceiver {
	static boolean isnoty = false;

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent intent2 = new Intent(context, MusicService.class);
		intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		MusicNum.putplay(3);
		MusicNum.putisok(true);
		// intent2.putExtra("play", 3);
		context.startService(intent2);
		isnoty = true;
	}

	public static boolean getnoti() {
		return isnoty;

	}

	public static void putnoti(boolean b) {
		isnoty = b;
	}

}